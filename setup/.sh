cd /Users/j2sdk_ball/workspace/money-table/mtcore/integrated-services
ln -s /Users/j2sdk_ball/workspace/money-table/mt-admin-api
cd ..
vagrant halt
vagrant plugin install vagrant-notify-forwarder
vagrant plugin install vagrant-notify
vagrant up
vagrant provision
vagrant ssh
sudo mt-services restart
mt-services seeddev

# sudo mt-services
# sudo mt-services status
# sudo mt-services log mt-admin-api
# sudo service mt-admin-api stop
# mount | grep mt-admin-api
# cd /goapp/integrated-services/mt-admin-api/
# nodemon app.js